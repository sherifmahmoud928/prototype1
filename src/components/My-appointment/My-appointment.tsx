import { Component, Method, State, h, Event, EventEmitter } from '@stencil/core'

@Component({
  tag: 'my-appointment',
  styleUrl: 'My-appointment.scss'
})
export class MyAppointment {
  @State() showOptions = false
  @Event() onClose: EventEmitter
  @Method()
  open() {
    this.showOptions = true
  }
  close() {
    this.showOptions = false
  }

  render() {
    var options = null
    if (!this.showOptions) {
      options = (
        <div class="col-3">
          <div class="card">
            <img
              src="https://s3.ap-south-1.amazonaws.com/m3india-app-dev/ckeditor/content/ticket-1551679693.jpg"
              class="card-img-top book_image"
              alt="..."
            />
            <div class="card-body">
              <h5 class="card-title">Book your appointment Online Now .</h5>
              <a onClick={this.open.bind(this)} class="btn btn-primary book_btn">
                Book Now
              </a>
            </div>
          </div>
        </div>
      )
    } else {
      options = (
        <div class="col main_book_con py-3">
          <div class="row justify-content-center">
            <div class="col-9 px-4 py-5 book_form">
              <div class="row">
                <div class="col">
                  <i onClick={this.close.bind(this)} class="far fa-window-close  float-right mb-3" />
                </div>
              </div>
              <div class="card">
                <form class="px-4 py-5">
                  <div class="form-group">
                    <label htmlFor="phone_number">Please Enter Your Phone Number .</label>
                    <input type="number" class="form-control" id="phone_number" placeholder="Phone Number" />
                    <small id="emailHelp" class="form-text text-muted">
                      We'll never share your Phone with anyone else.
                    </small>
                  </div>
                  <button type="submit" class="btn btn-primary">
                    Submit
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      )
    }
    return (
      <div class="container-fluid mt-5">
        <div class="row justify-content-center">
          <div class="col-3">
            <div class="card">
              <div class="card-header">Opining Hours</div>
              <div class="row">
                <div class="col-5">
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item">Monday</li>
                    <li class="list-group-item">Tuesday</li>
                    <li class="list-group-item">Wednesday</li>
                    <li class="list-group-item">Thursday</li>
                    <li class="list-group-item">Friday</li>
                    <li class="list-group-item">Saturday</li>
                    <li class="list-group-item">Sunday</li>
                  </ul>
                </div>
                <div class="col">
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item">9:00AM – 4:00PM</li>
                    <li class="list-group-item">9:00AM – 4:00PM</li>
                    <li class="list-group-item">9:00AM – 4:00PM</li>
                    <li class="list-group-item">9:00AM – 4:00PM</li>
                    <li class="list-group-item">9:00AM – 4:00PM</li>
                    <li class="list-group-item">Colsed</li>
                    <li class="list-group-item">closed</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          {options}
        </div>
      </div>
    )
  }
}
