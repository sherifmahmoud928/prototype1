# my-appointment



<!-- Auto Generated Below -->


## Events

| Event     | Description | Type               |
| --------- | ----------- | ------------------ |
| `onClose` |             | `CustomEvent<any>` |


## Methods

### `open() => Promise<void>`



#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
